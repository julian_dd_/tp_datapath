-- ARQUITECTURA DE COMPUTADORAS
-- ALUMNOñ JULIAN DELLE DONNE
-- AÑO:2022

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UC is
    generic(
        opcode  :   integer :=  32
    );
    port(
        INSTR_i         :   in  std_logic_vector(opcode-1 downto 0);            -- instruccion de entrada
        -- * Condbranch_o    :   out std_logic;                                 -- salto
        branch_o        :   out std_logic;                                      -- salto condicional
        -- * uncondbranch_o  :   out std_logic;                                 -- salto incondicional
        MemRead_o       :   out std_logic;                                      -- lectura de memoria
        MemtoReg_o      :   out std_logic;                                      -- memoria a registro
        ALUop_o         :   out std_logic_vector(3 downto 0);              		-- seleccion de operacion de ALU
        MemWrite_o      :   out std_logic;                                      -- escritura de memoria
        ALUsrc_o        :   out std_logic;                                      -- selecciona entre un inmediato y un registro
        -- * U_type_o        :   out std_logic;                                 -- permite utilizar lui
        Reg_W_o         :   out std_logic                                       -- escritura de registro                                     
    );
end UC;

architecture control_arch of UC is
    signal opcode_i:    std_logic_vector(6 downto 0);                           -- opcode de instruccion, hasta 7 bits
    signal func3:       std_logic_vector(2 downto 0);                           -- func3 de instruccion, hasta 3 bits 
    signal func7:       std_logic_vector(6 downto 0);                           -- func7 de instruccion, hasta 7 bits 

    begin
    opcode_i    <= INSTR_i(6 downto 0);                                         -- se le otorga la parte que corresponde al opcode
    func3       <= INSTR_i(14 downto 12);                                       -- se le ortorga la parte que corresponde a func3
    func7       <= INSTR_i (31 downto 25);                                      -- se le otorga la parte qu corresponde a func7   
    -- * Condbranch_o        <= '1' when (opcode_i = "1100111") else '0';
    branch_o    <= '1' when (opcode_i = "1100111") else '0';
    -- * uncondbranch_o  <= '1' when (opcode_i= "1100011" or opcode_i="1101111") else '0';
    MemRead_o   <= '1' when (opcode_i = "0000011") else '0';
    MemtoReg_o  <= '1' when (opcode_i = "0000011") else '0';
    MemWrite_o  <= '1' when (opcode_i = "0100011") else '0';
    ALUsrc_o    <= '1' when (opcode_i="0000011" or opcode_i = "0010011" or opcode_i= "0100011") else '0'; -- verificar jalr y jal 
    Reg_W_o     <= '1' when (opcode_i="0110011" or opcode_i="0000011" or opcode_i= "0010011") else '0';
    
--------------------------------------------------------------------------------
-- ALUop_o 
-- 0000 ADD (instrucciones= add, addi, ldur, stur)
-- 0001 SUB (sub, subs)
-- 0011 AND (and, andi)
-- 0100 OR  (or, ori)
-- 0101 XOR (xor, xori)
-- 0110 SRL
-- 0111 SRA 
-- 1000 SLL 

--0000";   -- AND Logico
--0001";   -- OR Lógico
--0010";   -- SUMA
--0101";   -- XOR Lógico
--0110";   -- RESTA
--1000";   --  Shifteo izquierdo lógico
--1001";   --  Shifteo derecho lógico
--1010";   --  Shifteo izquierdo aritmetico
--1011";   --  Shifteo derecho aritmético
-- 0011    * EQ
-- 0100    * ENQ
-- 0111    * BLT
-- 1100    * BGE
-- 1101    * BLTU
-- 1110    * BGEU
----------------------------------------------------------------------------------
--ALUop_o <=  "0010" when ((opcode_i = "0110011" and func3="000" and func7="0000000") or (opcode_i="0010011" and func3="000"))else --add, addi
--           "0110" when ((opcode_i = "0110011" and func7="0100000") or (opcode_i="1100111")) else --sub, subs
--            "1011" when ((opcode_i = "0110011" and func3="101") or (opcode_i="0010011" and func3="101" and func7="0100000"))else --srl, sra
--            "0101" when ((opcode_i = "0110011" and func3="100") or (opcode_i="0010011" and func3="100"))else --xor, xori
--            "1001" when ((opcode_i = "0110011" and func3="101") or (opcode_i="0010011" and func3="101" and func7="0000000"))else
--            "0001" when ((opcode_i = "0110011" and func3="110") or (opcode_i="0010011" and func3="110"))else --or, ori
--            "1000" when ((opcode_i = "0110011" and func3="001" and func7="0000000") or (opcode_i="0010011" and func3="001" and func7="0000000")) else
----------------------------------------------------------------------------------
ALUop_o <=  "0010" when (opcode_i = "0110011" and func3="000" and func7="0000000") else -- ADD      
            "0010" when (opcode_i = "0010011" and func3="000") else                     -- ADDI     
            "0110" when (opcode_i = "0110011" and func3="000" and func7="0100000") else -- SUB      
            "1000" when (opcode_i = "0110011" and func3="001" and func7="0000000") else -- SLL      
            "1000" when (opcode_i = "0010011" and func3="001" and func7="0000000") else -- SLLI
            "0101" when (opcode_i = "0110011" and func3="100" and func7="0000000") else -- XOR      
            "0101" when (opcode_i = "0010011" and func3="100") else                     -- XORI
            "0001" when (opcode_i = "0110011" and func3="110" and func7="0000000") else -- OR       
            "0001" when (opcode_i = "0010011" and func3="110") else                     -- ORI 
            "1001" when (opcode_i = "0110011" and func3="101" and func7="0000000") else -- SRL
            "1001" when (opcode_i = "0010011" and func3="101" and func7="0000000") else -- SRLI
            "1011" when (opcode_i = "0110011" and func3="101" and func7="0000000") else -- SRA
            "1011" when (opcode_i = "0010011" and func3="101" and func7="0100000") else -- SRAI
            "0000" when (opcode_i = "0110011" and func3="111" and func7="0000000") else -- AND
            "0000" when (opcode_i = "0010011" and func3="111") else                     -- ANDI             
            "1100" when (opcode_i = "1100111" and func3="000") else                     -- EQ       
            "1101" when (opcode_i = "1100111" and func3="001") else                     -- NEQ      
            "1110" when (opcode_i = "1100111" and func3="100") else                     -- BLT
            "1111" when (opcode_i = "1100111" and func3="101") else                     -- BGE            
            "0111" when (opcode_i = "1100111" and func3="110") else                     -- BLTU
            "0011" when (opcode_i = "1100111" and func3="111") else                     -- BGEU
            "0000";

-- * U_type_o <= '1' when (opcode_i="0110111") else '0';

end control_arch;
