-- ARQUITECTURA DE COMPUTADORAS
-- ALUMNO: JULIAN DELLE DONNE
-- AÑO: 2022

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity datamem is
generic (
        ADDR_WIDTH : integer := 10;  
        MEMO_SIZE  : integer := 1024;
        DATA_WIDTH : integer := 32
        );
port (
        CLK_i      : in  std_logic;
        ADDR_i     : in  std_logic_vector (ADDR_WIDTH-1 downto 0);
        DATA_i     : in  std_logic_vector (DATA_WIDTH-1 downto 0);
        DATA_o     : out std_logic_vector (DATA_WIDTH-1 downto 0);
        MemWrite   : in  std_logic;
        MemRead    : in  std_logic  
    );
    
end entity datamem;


architecture datamem_arch of datamem is

    type t_array_mem is array(MEMO_SIZE-1 downto 0) of std_logic_vector (DATA_WIDTH-1 downto 0);
    signal memory_s : t_array_mem;
    signal zeroes   : std_logic_vector(DATA_WIDTH-1 downto 0);

begin
        zeroes <= (others => '0');
        with MemRead select
                DATA_o <= memory_s(to_integer(unsigned(ADDR_i))) when '1', zeroes when others;

    write_loogic_process:
    process (CLK_i) is
    begin    
        if (rising_edge(CLK_i)) then  
            if (MemWrite = '1') then
                memory_s(to_integer(unsigned(ADDR_i))) <= DATA_i;
            end if;
        end if;
    end process;

end architecture datamem_arch;
