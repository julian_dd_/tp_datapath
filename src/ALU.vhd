-- ARQUITECTURA DE COMPUTADORAS
-- ALUMNO: JULIAN DELLE DONNE
-- AÑO: 2022

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu is

generic (N : integer :=32);
port (
      A_i      : in  std_logic_vector(N - 1 downto 0);
      B_i      : in  std_logic_vector(N - 1 downto 0);
      OUTPUT_o : out std_logic_vector(N - 1 downto 0);
      ALUop_i  : in  std_logic_vector(3 downto 0);
      Zero_o   : out std_logic
      );
end entity;

architecture alu_arch of alu is

constant AND_IN : std_logic_vector(3 downto 0) := "0000"; -- AND Logico
constant OR_IN  : std_logic_vector(3 downto 0) := "0001"; -- OR Lógico                          
constant ADD    : std_logic_vector(3 downto 0) := "0010"; -- SUMA                               
constant XOR_IN : std_logic_vector(3 downto 0) := "0101"; -- XOR Lógico                         
constant SUB    : std_logic_vector(3 downto 0) := "0110"; -- RESTA                              
constant SLL_IN : std_logic_vector(3 downto 0) := "1000"; -- Shifteo izquierdo lógico          
constant SRL_IN : std_logic_vector(3 downto 0) := "1001"; -- Shifteo derecho lógico            
constant SLA_IN : std_logic_vector(3 downto 0) := "1010"; -- Shifteo izquierdo aritmetico
constant SRA_IN : std_logic_vector(3 downto 0) := "1011"; -- Shifteo derecho aritmético        
constant EQ_IN  : std_logic_vector(3 downto 0) := "1100"; -- Saltar si A y B son iguales
constant NEQ_IN : std_logic_vector(3 downto 0) := "1101"; -- Saltar si A y B no son iguales
constant BLT_IN : std_logic_vector(3 downto 0) := "1110"; -- Saltar si A es menor que B (con signo)
constant BGE_IN : std_logic_vector(3 downto 0) := "1111"; -- Saltar si A es mayor igual que B (con signo)
constant BLTU_IN: std_logic_vector(3 downto 0) := "0111"; -- Saltar si A es menor que B (sin signo)
constant BGEU_IN: std_logic_vector(3 downto 0) := "0011"; -- Saltar si A es mayor igual que B (sin signo)

signal output_s : std_logic_vector(N-1 downto 0);
signal zero: std_logic;

begin

OUTPUT_o <= output_s;
Zero_o <= zero;

-- * Zero_o <= '1' when output_s = std_logic_vector(to_unsigned(0,N)) else '0'; 

process(A_i, B_i, ALUop_i)

variable aux1 : std_logic;
variable aux2 : std_logic_vector(N-1 downto 0);

begin
    case ALUop_i is
        when ADD =>
            zero <= '0';
            output_s <= std_logic_vector((unsigned(A_i) + unsigned(B_i)));
        when SUB =>
            zero <= '0';
            output_s <= std_logic_vector((unsigned(A_i) - unsigned(B_i)));
        when AND_IN =>
            zero <= '0';
            output_s <= A_i and B_i;
        when OR_IN =>
            zero <= '0';
            output_s <= A_i or B_i;
        when XOR_IN =>
            zero <= '0';
            output_s <= A_i xor B_i;
        when SLL_IN =>
            zero <= '0';
            output_s <= std_logic_vector(shift_left(unsigned(A_i), to_integer(unsigned(B_i))));
        when SRL_IN =>
            zero <= '0';
            output_s <= std_logic_vector(shift_right(unsigned(A_i), to_integer(unsigned(B_i))));
        when SLA_IN =>
       	    zero <= '0';
            output_s <= std_logic_vector(shift_left(signed(A_i), to_integer(unsigned(B_i))));
        when SRA_IN =>
            zero <= '0';
            output_s <= std_logic_vector(shift_right(signed(A_i), to_integer(unsigned(B_i))));
        when EQ_IN =>
            output_s <= (others =>'0');		
            if(A_i = B_i) then
            	zero <= '1';
            else
            	zero <= '0';
            end if;	
        when NEQ_IN =>
            output_s <= (others =>'0');
            if(A_i /= B_i) then
            	zero <= '1';
            else
            	zero <= '0';	
            end if;
        when BLT_IN =>
            output_s <= (others =>'0');
            if(A_i < B_i) then
            	zero <= '1';
            else
            	zero <= '0';	
            end if;
        when BGE_IN =>
            output_s <= (others =>'0');
            if(A_i >= B_i) then
            	zero <= '1';
            else 
            	zero <= '0';	
            end if;
        when BLTU_IN =>
            output_s <= (others =>'0');
            if(unsigned(A_i) < unsigned(B_i)) then
            	zero <= '1';
                output_s(0) <= '1';
            else
            	zero <= '0';	
            end if;
        when BGEU_IN =>
            output_s <= (others =>'0');
            if(unsigned(A_i) >= unsigned(B_i)) then
            	zero <= '1';
                output_s(0) <= '1';
            else
            	zero <= '0';	
            end if;
        
        when others => output_s <= (others => '0');        
    end case;
        
    end process;

end alu_arch;
