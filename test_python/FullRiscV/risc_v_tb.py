import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure

CLK_PERIOD = 10

@cocotb.coroutine
def Reset(dut):
    dut.RST_i <=  0
    yield Timer(CLK_PERIOD * 1)
    dut.RST_i  <= 1
    yield Timer(CLK_PERIOD * 10)
    dut.RST_i  <= 0

@cocotb.test()
def test(dut):
    """
    Description:
        Test RISC-V Simple Implementation
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())
    
    yield Reset(dut)

    #yield Timer(CLK_PERIOD * 64)

    for i in range(20):
        yield RisingEdge(dut.CLK_i)
        opcode = ""
        tipo = ""
        print("Instrucción:",dut.Instruction.value)
            
        opcode += str(dut.Instruction.value[25:31])
        print("\t")

        print("PC out: ",dut.PCOUT.value.integer)
        
        print("\t")
        if(opcode == "0110011"):
            print("Instrucción tipo R")
            tipo = "R"
        elif(opcode == "0100011"):
            print("Instrucción tipo S")
            tipo = "S"
        elif(opcode == "1100111"):
            print("Instrucción tipo SB")
            tipo = "SB"
        elif(opcode == "0000011" or opcode == "0010011" or opcode == "1100111"):
            print("Instrucción tipo I")
            tipo = "I"
        else:
            print("Formato Invalido")
        print("\t")

        print("Inm shifted left in:",dut.ImmGenOUT.value)
        print("Inm shifted left out:",dut.SHIFTED_LEFT.value)
        print("\t")
        
        print("CU:________")
        print("Branch:",dut.Branch.value)
        print("MemRead:",dut.MemRead.value)
        print("MemtoReg:",dut.MemtoReg.value)
        print("MemWrite:",dut.MemWrite.value)
        print("ALUSrc:",dut.ALUSrc.value)
        print("RegWrite:",dut.RegWrite.value)
        print("\n")
        print("Banco de registros:________")

        print("RS1: " + "X" + str(dut.Instruction.value[12:16].integer))
        
        if(tipo == "I"):
            if(str(dut.Instruction.value[0:5]) == "010000" or str(dut.Instruction.value[0:5]) == "000000"):
                print("Inm: " + str(dut.Instruction.value[6:11].integer))
            else:
                print("Inm: " + str(dut.Instruction.value[0:11].integer))
            print("RD: " + "X" + str(dut.Instruction.value[20:24].integer))
        
        elif(tipo == "S"):
            print("RS2: " + "X" + str(dut.Instruction.value[7:11].integer))
            #inmed = str(dut.Instruction.value[0:6]) + str(dut.Instruction.value[20:24])
            inmed_int = str(dut.Instruction.value[0:6].integer) + str(dut.Instruction.value[20:24].integer)
            print("Inm: "+ inmed_int)
        
        elif(tipo == "SB"):
            print("RS2: " + "X" + str(dut.Instruction.value[7:11].integer))
            #inmed = str(dut.Instruction.value[0]) + str(dut.Instruction.value[24])  + str(dut.Instruction.value[1:6]) + str(dut.Instruction.value[20:23])
            inmed_int = str(dut.Instruction.value[0].integer) + str(dut.Instruction.value[24].integer)  + str(dut.Instruction.value[1:6].integer) + str(dut.Instruction.value[20:23].integer)
            print("Inm: "+ inmed_int)
        
        else: #Tipo R
            print("RS2: " + "X" + str(dut.Instruction.value[7:11].integer))
            print("RD: " + "X" + str(dut.Instruction.value[20:24].integer))

        print("Reg1 Out:", dut.ReadDATA1toALU.value)
        print("Reg2 Out:", dut.ReadDATA2toALU.value)
        print("W_c_i:", dut.MUXtoREGISTERS.value),
        print("\n")    
        print("ALU:________")
        print("A_i:", dut.ReadDATA1toALU.value)
        print("B_i:", dut.MuxtoALU.value)
        print("OUTPUT:",dut.ALUresult.value)
        print("Zero:",dut.ZERO.value)
        print("Operacion: ", dut.ALUOp.value,end=""),
        print("\n")      
        print("Memoria de datos:________")
        print("DATA_i:",dut.ReadDATA2toALU.value)
        print("ADDR_i:",dut.ALUresult.value)
        print("DATA_o:",dut.DataMEMtoMux.value)
        print("MemWrite:",dut.MemWrite.value)
        print("MemRead:",dut.MemRead.value),  
        print("-----------------------------------------------------------")
        print("\n")




