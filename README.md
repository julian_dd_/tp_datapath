# ARQUITECTURA DE COMPUTADORAS
Alumno: Delle Donne Julian

Profesor responsable: Andres Miguel Airabella 

Diseño de Datapath para el set de instrucciones RISC-V:


A continuacion se muestran los bloques que se utilizaran para realizar el datapath del set de instrucciones RISC-V


 <img src="Bloques/ALU.svg" width="200">

 <img src="Bloques/Banco de Registros.svg" width="250">

 <img src="Bloques/Contador de Programa.svg" width="250">

 <img src="Bloques/Memoria de datos.svg" width="300">

 <img src="Bloques/Memoria de programa.svg" width="300">

 <img src="Bloques/Multiplexor.svg" width="150">

 <img src="Bloques/and.svg" width="200">

 <img src="Bloques/suma.svg" width="150">



​	**Realice el dibujo de un Datapath completo para el set de instrucciones propuesto. Indique en el dibujo anchos de todos los buses y nombres de las señales intermedias que luego utilizará en el código.** 


Teniendo en cuenta los bloques anteriores se procede a armar un datapath de forma progresiva que pueda realizar un subconjunto de las instrucciones dadas e ir agregando paso a paso otro subconjunto hasta conformar el datapath final. Se comienza modelando un procesador que pueda realizar operaciones entre registros (tipo R). Las operaciones que podra realizar son las siguientes:

* Aritméticas: ADD SUB
* Bits: AND OR XOR
* Comparaciones: SLT SLTU
* Desplazamientos: SLL SRL SRA

 <img src="Bloques/operaciones_entre_registros.png" width="1000">


El siguiente paso es hacer que el procesador pueda realizar operaciones de computos con constantes (tipo I), de esta forma a las instrucciones mencionadas anteiormente se le agregan liguientes:

* Aritméticas: ADDI
* Bits: ANDI ORI XORI
* Comparaciones: SLTI SLTUI
* Desplazamientos: SLLI SRLI SRAI


 <img src="Bloques/computo_con_constantes.png" width="1000">


Continuando con el armado del datapath final se procede a agregar operaciones de carga a memoria (tipo I) agregando la instruccion de carga de memoria a registro LW. Para ello es necesario utilizar el bloque memoria de datos como se observa a continuacion:


 <img src="Bloques/carga_a_memoria.png" width="1000">


Agregando las instrucciones de almacenamiento en memoria (tipo S) el dataspath queda de la siguiente forma:


 <img src="Bloques/almacenamiento_en_memoria.png" width="1000">


Finalmente, se agrgan las instrucciones de branch (tipo B) y el contador de programa. El resultado final es el siguiente:


 <img src="Bloques/datapath_final.png" width="1000">


A continuacion se detalla la tabla de verdad para la unidad de control (CU) la cual permitira direccionar las señales de control para realizar los diferentes tipos de instrucciones mencionados anteriormente.


|      |MemtoReg|MemRead|MemWrite|ALUSrc|Reg_W_i|Branch|
|:----:|:------:|:-----:|:------:|:----:|:-----:|:----:|
|Tipo R|0|0|0|0|1|0|
|Tipo I|0|0|0|1|1|0|
|Tipo LW|1|1|0|1|1|0|
|Tipo SW|1|0|1|1|0|0|
|Tipo SB|X|0|0|1|0|1|

La tabla de verdad para el control de la ALU es la siguiente:

|      |ALUop_i[3]|ALUop_i[2]|ALUop_i[1]|ALUop_i[0]|
|:----:|:--------:|:--------:|:--------:|:--------:|
|AND_IN|0|0|0|0|
|OR_IN|0|0|0|1|
|ADD|0|0|1|0|
|XOR_IN|0|1|0|1|
|SUB|0|1|1|0|
|SLL_IN|1|0|0|0|
|SRL_IN|1|0|0|1|
|SLA_IN|1|0|1|0|
|SRA_IN|1|0|1|1|
|EQ_IN|1|1|0|0|
|NEQ_IN|1|1|0|1|
|BLT_IN|1|1|1|0|
|BGE_IN|1|1|1|1|
|BLTU_IN|0|1|1|1|
|BGEU_IN|0|0|1|1|

Se decidio no implementar las instrucciones jal y jalr ya que las mismas pueden ser realizadas utilizando instrucciones alternativas como saltos condicionales (EQ) en donde los dos registros a comparar son los mismos, de esta forma se obtiene un salto "incondicional" donde el resultado de la comparacion es verdadera. Ademas, las ventajas de este tipo de saltos que permiten un desplazamiento mucho mayor no se ve reflejada ya que la memoria implementada no es de grandes dimenciones. 


A continuacion se muestra la tabla realizada para probar el correcto funcionamiento de las instrucciones del procesador, la misma se puede descargar del archivo tabla_instrucciones.ods.

|ARQUITECTURA DE COMPUTADORAS| | | | | | | | | | |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Alumno: JULIAN DELLE DONNE| | | | | | | | | | |
| | | | | | | | | | | |
|Instrucción|Tipo R|Func7 7 bits|Rs2 5 bits|Rs1 5 bits|Func3 3 bits|Rd 5 bits|Opcode 7 bits|OPERACIÓN|RESS|.txt|
| |Tipo I|Inm 7 bits|Inm 5 bits|Rs1 5 bits|Func3 3 bits|Rd 5 bits|Opcode 7 bits| | | |
| |Tipo S|Inm 7 bits|Rs2 5 bits|Rs1 5 bits|Func3 3 bits|Inm 5 bits|Opcode 7 bits| | | |
| |Tipo SB|Inm 7 bits|Rs2 5 bits|Rs1 5 bits|Func3 3 bits|Inm 5 bits|Opcode 7 bits| | | |
| | | | | | | | | | | |
|ADDI|I|0000000|00001|00000|000|00001|0010011|x1 <= x0 + 1|x1 <= 1|00000000|
| | | | | | | | | | |00010000|
| | | | | | | | | | |00000000|
| | | | | | | | | | |10010011|
|ADDI|I|0000000|00010|00000|000|00010|0010011|x2 <= x0 + 2|x2 <= 2|00000000|
| | | | | | | | | | |00100000|
| | | | | | | | | | |00000001|
| | | | | | | | | | |00010011|
|ADDI|I|0000000|00011|00000|000|00011|0010011|x3 <= x0 + 3|x3 <= 3|00000000|
| | | | | | | | | | |00110000|
| | | | | | | | | | |00000001|
| | | | | | | | | | |10010011|
|ADDI|I|0000000|00001|00011|000|00100|0010011|x4 <= x3 + 1|x4 <= 4|00000000|
| | | | | | | | | | |00010001|
| | | | | | | | | | |10000010|
| | | | | | | | | | |00010011|
|ADD|R|0000000|00011|00010|000|00101|0110011|x5 < = x2 + x3|x5 <= 5|00000000|
| | | | | | | | | | |00110001|
| | | | | | | | | | |00000010|
| | | | | | | | | | |10110011|
|XOR|R|0000000|00101|00011|100|00110|0110011|x6 <= x5 xor x3|x6 <= 6|00000000|
| | | | | | | | | | |01010001|
| | | | | | | | | | |11000011|
| | | | | | | | | | |00110011|
|OR|R|0000000|00010|00101|110|00111|0110011|x7<= X2 or x5|x7 <= 7|00000000|
| | | | | | | | | | |00100010|
| | | | | | | | | | |11100011|
| | | | | | | | | | |10110011|
|SLL|R|0000000|00001|00011|001|00110|0110011|x6 <= x3 << 1|x6 <= 6|00000000|
| | | | | | | | | | |00010001|
| | | | | | | | | | |10010011|
| | | | | | | | | | |00110011|
|EQ|SB|0000000|00011|00011|000|00010|1100111|if (x3= x3) go to PC+4*2| |00000000|
| | | | | | | | |SALTA| |00110001|
| | | | | | | | | | |10000001|
| | | | | | | | | | |01100111|
|AND|R|0000000|00010|00111|111|00010|0110011|x2<= x2 and x7|X2 <= 2|00000000|
| | | | | | | | | | |00100011|
| | | | | | | | | | |11110001|
| | | | | | | | | | |00110011|
|SUB|R|0100000|00010|00011|000|00001|0110011|x1 <= x2-x3|X1 <= 1|01000000|
| | | | | | | | | | |00100001|
| | | | | | | | | | |10000000|
| | | | | | | | | | |10110011|
|NEQ|SB|0000000|00011|00011|001|00011|1100111|if (x3!= x3) go to PC+4*3| |00000000|
| | | | | | | | |NO SALTA| |00110001|
| | | | | | | | | | |10010001|
| | | | | | | | | | |11100111|
|SD|S|0000000|00100|00010|111|00111|0100011| | |00000000|
| | | | | | | | | | |01000001|
| | | | | | | | | | |01110011|
| | | | | | | | | | |10100011|
|LD|I|0000000|00010|00010|011|00100|0000011| | |00000000|
| | | | | | | | | | |00100001|
| | | | | | | | | | |00110010|
| | | | | | | | | | |00000011|
|SUB|R|0100000|00010|00011|000|00001|0110011|x1 <= x2-x3|x1 <= 1|01000000|
| | | | | | | | | | |00100001|
| | | | | | | | | | |10000000|
| | | | | | | | | | |10110011|
|SW|S|0000000|00100|00010|010|00000|0100011| | |00000000|
| | | | | | | | | | |01000001|
| | | | | | | | | | |00100000|
| | | | | | | | | | |00100011|
|AND|R|0000000|00010|00111|111|00010|0110011|x2<= x2 and x7|x2 <= 2|00000000|
| | | | | | | | | | |00100011|
| | | | | | | | | | |11110001|
| | | | | | | | | | |00110011|
|BLT|SB|0000001|00010|00100|100|01000|1100111|if (x2< x4) go to PC+4*| |00000010|
| | | | | | | | |SALTA| |00100010|
| | | | | | | | | | |01000100|
| | | | | | | | | | |01100111|
|AND|R|0000000|00010|00111|111|00010|0110011|x2<= x2 and x7|x2 <= 2|00000000|
| | | | | | | | | | |00100011|
| | | | | | | | | | |11110001|
| | | | | | | | | | |00110011|


